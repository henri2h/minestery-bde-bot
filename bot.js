const logger = require('loglevel')

const stripHtml = require('string-strip-html')

const sdk = require('matrix-js-sdk')

// tweak to your own
const creds = require('./creds.json')
const clientOptions = creds

const { LocalStorage } = require('node-localstorage')
const localStorage = new LocalStorage('./local_storage/')
const regexConfigurationCommand = /\s+/ig

client = null

const CONSTS = {
  'FORBIDDEN_COMMAND': 'Not allowed to run this command, ask to moderator or administrator.',
  'WELCOME_TEXT': "Welcome in MINESTERY</br> Pour commander une crêpe tapez <bold>crepe</bold>.<br> Lors de la commande vous pouvez annuler à tout moment en envoyant <bold>annuler</bold>",
  'CHOIX_CHAMBRE': 'Lieu de rendez vous ? Addresse, chambre ?',
  'CHOIX_CREPE': 'Quelle crêpe voulez vous ? <br> Les garnitures disponibles sont : ',
  'MAUVAIS_CHOIX_CREPE': 'Ce choix de crepe n\'est pas possible',
  'AJOUT_CREPE': 'Voulez vous ajouter une autre crêpe à votre commande ? oui ou non',
  'COMMANDE_ENVOYEE': 'Votre commande à été envoyée. Nous vous enverrons un message lors de la prise en compte de celle-ci',
  'NO_COMMAND': 'Vous ne pouvez plus commander pour le moment, veuillez patienter'
}

const USER_STATUS = {
  'START': 'start',
  'SET_CHAMBRE': 'set_chambre',
  'SET_CREPE': 'set_crepe',
  'AJOUT': 'ajout',
  'VALIDATION': 'validation'
}

const CONVS = {
  'crepe': '!scNegkEfgAgprirdAz:emse.fr',
  'news': '!XtBafCknVpoxnNpjEv:emse.fr',
  'configuration': '!mudxPLSIfGYOBmdfkX:carnot.cc'
}

// data communication with server.js
const Data = require('./data.js')
const dataHelper = new Data()

var garnitures = []
var garnituresTexte = ''

async function start() {
  logger.setLevel("debug");
  if (!localStorage.getItem('accessToken')) {
    logger.debug('No stored access token found.')
    loginClient = sdk.createClient(clientOptions)
    loginClient.login('m.login.password', { 'user': creds.userId, 'password': creds.password, initial_device_display_name: 'Matrix webhooker' }).then((response) => {
      logger.debug('Successful login with credentials from creds.json.')
      localStorage.setItem('baseUrl', creds.baseUrl)
      localStorage.setItem('accessToken', response.access_token)
      localStorage.setItem('userId', response.user_id)
      localStorage.setItem('deviceId', response.device_id)
      loginClient.stopClient()
      startWithAccessToken()
    })
  } else {
    logger.debug('Stored access token found.')
    startWithAccessToken()
  }
  updateGarniture()
  if (localStorage.getItem('emergency') == null) {
    localStorage.setItem('emergency', true) // En cas de pb sur le fichier bloque par défaut
  }
}

async function startWithAccessToken() {
  clientOptions.accessToken = localStorage.getItem('accessToken')
  clientOptions.userId = localStorage.getItem('userId')
  clientOptions.deviceId = localStorage.getItem('deviceId')

  // STORES:
  clientOptions.sessionStore = new sdk.WebStorageSessionStore(localStorage)
  clientOptions.store = new sdk.MemoryStore()
  clientOptions.usingExternalCrypto = true

  client = sdk.createClient(clientOptions)

  // Enable the features to be used
  // autoAcceptInvites(client);
  //
  client.startClient({ initialSyncLimit: 2000 })
  const checkSyncState = (state, prevState, data) => {
    switch (state) {
      case 'PREPARED':
        logger.debug("Successful login with credentials from local storage.");
        client.getRooms();
        setupDefaultOptions();
        setupBindings();

        client.removeListener('sync', checkSyncState);

        console.log("load timeline");
        loadTimeline();
        autoJoin();
        console.log("Started");
        break;
      case 'ERROR':
        client.removeListener('sync', checkSyncState)
        break
      default:
    }
  }
  client.on('sync', checkSyncState)
}

function joinRoomAndSendMessage(roomId) {
  client.joinRoom(roomId).then((room) => {
    logger.debug("Auto-joined %s", room.roomId);
    sendMessageText(room.roomId, CONSTS.WELCOME_TEXT);
  });
}


function autoJoin() {
  let rooms = client.getRooms();
  for (var i = 0; i < rooms.length; i++) {
    if (rooms[i].getMyMembership() === 'invite') {
      console.log("Room name to join" + rooms[i].name);
      joinRoomAndSendMessage(rooms[i].roomId);
    }
  }
}


/**
 * Set the default options in case they aren't set.
 */
function setupDefaultOptions() {
  if (!localStorage.getItem('autoverify_unknown')) {
    localStorage.setItem('autoverify_unknown', true)
  }
}

function handleReactions(event) {
  if (event.getType() == 'm.reaction') {
    var e_relates_to = event.getContent()['m.relates_to']

    client.getRoom(event.event.room_id).timeline.forEach(t => {
      if (t.event.event_id == e_relates_to['event_id']) {
        var regex = /@([a-z0-9][-a-z0-9_\+\.]*[a-z0-9]):([a-z0-9][-a-z0-9\.]*[a-z0-9]\.(arpa|root|aero|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)|([0-9]{1,3}\.{3}[0-9]{1,3}))/g

        var reg = regex.exec(t.event.content.body)

        if (reg != null && reg.length > 0) {

          var userId = reg[0]
          getDirectRoom(userId, (roomId) => {
            if (e_relates_to['key'] == '👍️') {
              sendMessageText(roomId, '👍️ Votre commande à été prise en compte')
            } else if (e_relates_to['key'] == '🚀') {
              sendMessageText(roomId, '🚀 Votre commande est en trajet')
            }
            sendNotice(roomId, 'Récapitulatif : <br>' + t.event.content.body)
          })
        }
      }
    })
  }
}
async function handleEvent(event) {
  if (event.getRoomId() == CONVS.news) {
    await loadTimeline()
  }

  else if (event.getRoomId() == CONVS.configuration) {
    handleCommand(event);
  }
  else if (event.getType() !== 'm.room.message' && event.getRoomId() == CONVS.crepe) {
    handleReactions(event)
  }
}



function setupBindings() {

  client.on("Room.timeline", async function (event, room, toStartOfTimeline) {
    if (event.isEncrypted()) {
      // handling in handleEventDecrypted
      return
    }
    await handleEvent(event)
  })

  client.on('Event.decrypted', async function (event) {
    if (event.isDecryptionFailure()) {
      logger.error('Decryption failure: ' + event)
      return
    }
    await handleEvent(event)
  })

  // AutoJoin where bot is requested and save as direct_message room where it is such.
  client.on('RoomMember.membership', async function (event, member) {
    let mdirect_events = client.getAccountData('m.direct')
    const direct_events_content = mdirect_events ? mdirect_events.getContent() : {}

    var roomId = event.getRoomId();
    var room = client.getRoom(roomId);

    if (member.membership === "leave") {
      if (room.getInvitedAndJoinedMemberCount() == 2) { // takes time to take into account
        client.leave(event.getRoomId());
      }
      // If it can avoid the for because the user leaving
      // is not the bot it's better and faster going to the key in array directly
      if (direct_events_content[member.userId]) {
        const fast_dm_room_index = direct_events_content[member.userId].indexOf(event.getRoomId())
        if (fast_dm_room_index > -1) {
          mdirect_events.event.content[member.userId].splice(fast_dm_room_index, 1)
          client.setAccountData('m.direct', mdirect_events.getContent())
          return
        }
      }
      // Otherwise lookup for the room in all the m.direct conversations in accountdata
      for (const user_id in direct_events_content) {
        const dm_room_index = direct_events_content[user_id].indexOf(event.getRoomId())
        if (dm_room_index > -1) {
          // Remove from Account Data as DM if it is
          mdirect_events.event.content[user_id].splice(dm_room_index, 1)
          client.setAccountData('m.direct', mdirect_events.getContent())
          return
        }
      }
    }

    // User invites the bot, Need to auto-join and add to bot accountdata as m.direct
    // member.userId is the invited user, and here we want to be the bot
    if (member.membership === "invite" && member.userId === client.getUserId()) {
      autoJoin();
      joinRoomAndSendMessage(member.roomId);
    }

    // Bot invites User , Need to add to bot accountdata as m.direct if is_direct
    // Here we look for invitations which are sent by the bot
    if (member.membership === 'invite' && event.getSender() === client.getUserId()) {
      const peerUser = member.userId
      addDMAccountData(event, mdirect_events, direct_events_content, peerUser)
    }
  })
}

/**
 *
 * Adds Direct Message account data from an invitation event.
 *
 * @param {MatrixEvent} event - the event of the invitation.
 * @param {Object} mdirect_events - The existing map of m.direct.
 * @param {Object} direct_events_content - The existing m.direct events content
 * @param {string} peerUser - The user id who is inviting or we invite in a direct message
 *
 */
function addDMAccountData(event, mdirect_events, direct_events_content, peerUser) {
  if (event.getContent().is_direct && !direct_events_content[peerUser]) {
    if (mdirect_events) {
      // TODO if undefined when new user  it fails
      mdirect_events.event.content[peerUser] = []
    } else {
      var initDmMap = {
        content: {
          [peerUser]: [
          ]
        }
      }
    }
  }
  if (event.getContent().is_direct && (!direct_events_content[peerUser] || direct_events_content[peerUser].indexOf(event.getRoomId()) == -1)) {
    let fixDmMap = mdirect_events ? mdirect_events.event : initDmMap
    fixDmMap.content[peerUser].push(event.getRoomId())
    client.setAccountData('m.direct', fixDmMap.content)
  }
}

async function loadTimeline() {
  var room = client.getRoom(CONVS.news)

  const events = room.getLiveTimeline().getEvents()

  var r_timeline = []

  var to_del_ref = []
  for (let i = 0; i < events.length; i++) {
    var e = events[i]
    var content = e.getContent()

    if (e.getType() == 'm.room.message' && (content.msgtype == 'm.text' || content.msgtype == 'm.image' || e.isRedacted())) {
      var user = client.getUser(e.getSender())

      // delete history
      var relation = e.getRelation()
      if (relation != null && relation.rel_type == 'm.replace') {
        for (var iteration_num = 0; iteration_num < r_timeline.length; iteration_num++) {
          if (r_timeline[iteration_num] != null) {
            if (r_timeline[iteration_num].id == relation.event_id || r_timeline[iteration_num].ref == relation.event_id) {
              r_timeline.splice(iteration_num, 1)
            }
          }
        }
      }

      var event_body = content['body']
      if (content['m.new_content'] != null) {
        event_body = content['m.new_content'].body
      }
      var obj = {
        'id': e.getId(),
        'sender': e.getSender(),
        'sender_displayName': user.displayName,
        'content': event_body,
        'time': e.getDate(),
        'type': content.msgtype,
        'url': client.mxcUrlToHttp(user.avatarUrl, 128, 128, 'crop', true).replace('http://localhost:8010', 'https://matrix.carnot.cc')
      }

      if (content.msgtype == 'm.image') {
        obj['img_url'] = client.mxcUrlToHttp(content['url'], null, null, null, true).replace('http://localhost:8010', 'https://matrix.carnot.cc')
      }
      if (relation != null) {
        obj['ref'] = relation.event_id
      }

      if (!e.isRedacted()) {
        r_timeline.push(obj)
      } else {
        var ref_id = e.event.unsigned.redacted_because.redacts
        to_del_ref.push(ref_id)
      }
    }
  }

  for (var pos = 0; pos < to_del_ref.length; pos++) {
    var pos_iter = 0
    var ref_id = to_del_ref[pos]
    while (pos_iter < r_timeline.length) {
      if (r_timeline[pos_iter].ref == ref_id) {
        r_timeline.splice(pos_iter, 1)
      }
      pos_iter++
    }
  }

  dataHelper.saveNews(r_timeline)
}

/*
* Chambre -> crepe -> validation
 */
function getOrderSummary(commande, admin = false) {
  var text = ''

  if (admin) {
    text += 'Commande de ' + commande['user'] + ' le ' + commande['date'] + '<br>'
    text += commande['user_id'] + '<br>'
  }

  let counter = 0
  for (const [key, value] of Object.entries(commande['crepe'])) {
    counter += value
  }

  text += 'Chambre / adresse : ' + commande.chambre + '<br>'
  text += counter + ' crêpe(s) commandée(s) <br>'
  for (const [key, value] of Object.entries(commande['crepe'])) {
    text += '&bull; ' + value + ' ' + key + '<br>'
  }
  return text
}

function handleUserCommand(roomId, userId, text, eventId) {
  var lsCommandName = 'mistery_commande_' + userId
  var lsCommandStatus = 'mistery_status_command'

  var data = JSON.parse(localStorage.getItem(lsCommandStatus))
  if (data == null) data = {}

  var user_status = data[userId]

  var commande_data = JSON.parse(localStorage.getItem(lsCommandName))
  if (commande_data == null) commande_data = { 'crepe': {} }
  console.log(commande_data)
  var command_text = text.toLowerCase().trim()

  var message = ''

  if (localStorage.getItem('emergency') == true) {
    message += CONSTS.NO_COMMAND
  } else if (command_text == 'annuler') {
    // delete canceled crepe
    commande_data.crepe = {}
    user_status = USER_STATUS.START

    message += 'Commande annulée'
  } else {
    switch (user_status) {
      case null:
      case USER_STATUS.START:
        if (command_text == 'crepe' || command_text == 'crêpe') {
          user_status = USER_STATUS.SET_CHAMBRE
          message += CONSTS.CHOIX_CHAMBRE
          if (commande_data['chambre'] != null) {
            message += 'Votre adresse, chambre : ' + commande_data['chambre'] + '<br>' + 'ou entrez ok pour garder cette addresse'
          }
        } else {
          message += CONSTS.WELCOME_TEXT
        }
        break

      case USER_STATUS.SET_CHAMBRE:
        user_status = USER_STATUS.SET_CREPE

        if (command_text != 'ok') {
          commande_data['chambre'] = text
        } else {
          message += "Ok, on réutilise l'adresse mémorisée"
        }

        message += garnituresTexte
        break
      case USER_STATUS.SET_CREPE:
        if (garnitures.includes(command_text)) {
          user_status = USER_STATUS.AJOUT
          if (command_text in commande_data.crepe)
            commande_data.crepe[command_text]++
          else
            commande_data.crepe[command_text] = 1
          message += CONSTS.AJOUT_CREPE
        } else {
          message += CONSTS.MAUVAIS_CHOIX_CREPE
          message += garnituresTexte
        }
        break
      case USER_STATUS.AJOUT:
        if (command_text == 'oui') {
          user_status = USER_STATUS.SET_CREPE
          message += garnituresTexte
        } else if (command_text == 'non') {
          user_status = USER_STATUS.VALIDATION
          let date = new Date()
          commande_data['date'] = date.getDate() + '  à  ' + date.getHours() + ':' + date.getMinutes()
          commande_data['time'] = date.getTime()
          var user = client.getUser(userId)
          commande_data['user_id'] = userId
          commande_data['user'] = user.displayName

          message += 'Récapitulatif de la commande<br>' + getOrderSummary(commande_data) + '<br>Validez vous la commande ? oui/non'
        } else {
          message += CONSTS.AJOUT_CREPE
        }
        break
      case USER_STATUS.VALIDATION:
        if (command_text == 'oui') {
          user_status = 'start'

          commande_data['id'] = eventId;
          commande_data['order_status'] = 'sent'
          message += CONSTS.COMMANDE_ENVOYEE
          sendMessageText(CONVS.crepe, getOrderSummary(commande_data, true))

          dataHelper.writeCrepeCommand(commande_data)
          dataHelper.addCommand(commande_data, (err) => {
            if (err) {
              console.error(err.message)
            }
          })
          commande_data.crepe = {}

        } else if (command_text == 'non') {
          user_status = USER_STATUS.START
          commande_data = null
          message += 'Commande annulée'
        } else {
          message += 'Récapitulatif de la commande<br>' + getOrderSummary(commande_data) + '<br>souhaitez vous confirmer votre commande ? oui/non'
        }
        break
      default:
        user_status = USER_STATUS.START
        message += 'Une erreur est survenue. Commande annulée'
        commande_data.crepe = {}
        break

    }
  }

  data[userId] = user_status
  localStorage.setItem(lsCommandStatus, JSON.stringify(data))
  localStorage.setItem(lsCommandName, JSON.stringify(commande_data))

  sendMessageText(roomId, message)
}

/**
 *  This will handle the command in msg and run the apropiate method.
 */
async function handleMessage(event) {
  // Basic command where we are only interested in messages from the test room, which start with "!echo",
  if (event.getSender() != creds.userId) {
    var room = client.getRoom(event.getRoomId())
    let members = room.currentState.getMembers().filter((m) => {
      return m.userId !== creds.userId && m.membership === 'join'
    })
    if (members.length == 1) {
      handleUserCommand(event.getRoomId(), event.getSender(), event.getContent().body, event.event.event_id)
    }
  }

  if (event.getContent().body.startsWith('!rst')) {
    if (canRunCommand(event.getRoomId(), event.getSender())) {
      console.log('reset event')
    }
  }
}
function handleCommand(event) {
  var command_text = ""
  if (event.getContent() != undefined) command_text = event.getContent().body.toLowerCase().trim()
  var parameters = command_text.replace(regexConfigurationCommand, ' ').split(' ')

  logger.info('Parameters ' + parameters) // DEBUG

  if (event.getSender() != creds.userId) {
    if (parameters.length > 0 && parameters[0].charAt(0) == '!') {
      switch (parameters[0]) {
        case '!g': // g stand for garnitures
          if (parameters.length < 2) {
            logger.debug('Not enought parameters !')
            sendMessageText(CONVS.configuration, 'Not enought parameters !')
            break;
          }

          switch (parameters[1]) {
            case "add":
              if (parameters.length < 3) {
                logger.debug('Not enought parameters !')
                sendMessageText(CONVS.configuration, 'Not enought parameters !')
                break;
              }
              for (var i = 2; i < parameters.length; i++)
                if (!garnitures.includes(parameters[i]))
                  garnitures.push(parameters[i])
              logger.debug('Add garnitures' + JSON.stringify(parameters.slice(2)))
              sendMessageText(CONVS.configuration, 'Add garnitures' + JSON.stringify(parameters.slice(2)))
              break;
            case "rm":
              if (parameters.length < 3) {
                logger.debug('Not enought parameters !')
                sendMessageText(CONVS.configuration, 'Not enought parameters !')
                break;
              }
              for (var i = 2; i < parameters.length; i++)
                for (var j = 0; j < garnitures.length; j++)
                  if (garnitures[j] == parameters[i])
                    garnitures.splice(j, 1)
              logger.debug('Remove garnitures' + JSON.stringify(parameters.slice(2)))
              sendMessageText(CONVS.configuration, 'Remove garnitures' + JSON.stringify(parameters.slice(2)))
              break;
            case "set":
              garnitures = parameters.slice(2)
              logger.debug('Set garnitures' + JSON.stringify(parameters.slice(2)))
              sendMessageText(CONVS.configuration, 'Set garnitures' + JSON.stringify(parameters.slice(2)))
              break;
            case "ls":
              sendMessageText(CONVS.configuration, JSON.stringify(garnitures))
              logger.debug('Current garnitures' + JSON.stringify(garnitures))
              break;
            default:
              sendMessageText(CONVS.configuration, 'Commande non reconnue !')
              logger.debug('Commande non reconnue ! dans ' + CONVS.configuration)
              break;
          }
          dataHelper.saveGarnitureCrepe(garnitures);
          updateGarniture()
          break;
        case '!reinit':
          logger.debug('Reinit !')
          sendMessageText(CONVS.configuration, 'Reinit !')
          autoJoin();
          loadTimeline();
          updateGarniture();
          break;
        case '!emerg':
          if (parameters.length < 2) {
            logger.debug('Not enought parameters !')
            sendMessageText(CONVS.configuration, 'Not enought parameters !')
            break;
          }
          switch (parameters[1]) {
            case 'on':
              localStorage.setItem('emergency', true)
              break;
            case 'off':
              localStorage.setItem('emergency', false)
              break;
            case 'toggle':
              localStorage.setItem('emergency', localStorage.getItem('emergency'))
              break;
            case 'status':
              logger.debug('Emergency shift : ' + localStorage.getItem('emergency'))
              sendMessageText(CONVS.configuration, 'Emergency shift : ' + localStorage.getItem('emergency'))
              break;
            default:
          }
      }
    }
  }
}
/**
 *
 * Check if user has enough power level to run moderator command in specific room.
 *
 * @param {string} room_id - room id.
 * @param {string} user_id - user id.
 *
 * @return {bool}  about user is allowed or not
 */
function canRunCommand(room_id, user_id) {
  const room = client.getRoom(room_id)
  // obtain room member as sender fails to get correct powerLevel
  const room_member = room.getMember(user_id)
  // Get the current state of the room ('f'(forward)) is to get state in newest event
  // of timeline.
  // const room_state = room.getLiveTimeline().getState("f"); // TODO ¿ why not ?
  room_state = room.currentState

  // Only users of power level > 50 can use this command.
  //
  // This way we can check at current room state if user has sufficient power level
  // to redact events other than their own (moderate or administrate : power level > 50)
  // if (room_state.maySendStateEvent("m.room.bot.options", event.sender.userId)){ // TODO the state event power_levels needs to be present in this room https://matrix-org.github.io/matrix-js-sdk/0.6.1/models_room-state.js.html#line117  also https://github.com/matrix-org/matrix-react-sdk/blob/9aff2e836e5d2459e28fdb4af823be4f7441b3f0/src/ScalarMessaging.js#L418
  if (room_state._hasSufficientPowerLevelFor('redact', room_member.powerLevel)) {
    return true
  } else {
    sendNotice(room_id, CONSTS.COMMAND_FORBIDDEN)
    return false
  }
}

/**
 * Sends a command to room.
 *
 * @param {string} room_id
 *   The room id for which this administrative task is performed
 * @param {string} body
 *   The body of the message wishing to be sent.
 * @param {string} body_text
 *   The body of the message wishing to be sent in plain text.
 */
async function sendNotice(room_id, body, body_text) {
  var content = {
    'body': body_text + ' and ' + body_text,
    'format': 'org.matrix.custom.html',
    'formatted_body': body,
    'msgtype': 'm.notice'
  }

  logger.debug('Sending ' + body + ' to ' + room_id)
  await client.sendMessage(room_id, content).catch((error) => {
    logger.error('Something wrong...')
  })
}

function sendMessageText(room_id, text) {
  var content = {
    'body': text,
    'format': 'org.matrix.custom.html',
    'formatted_body': text,
    'msgtype': 'm.text'
  }
  client.sendMessage(room_id, content)
}

function updateGarniture() {
  logger.debug('Upadate garnitures')
  garnitures = dataHelper.getGarnitureCrepe()
  garnituresTexte = CONSTS.CHOIX_CREPE
  for (var i = 0; i < garnitures.length; i++) {
    garnituresTexte += ' ' + garnitures[i]
  }
}

/**
 * Sends a command to user id to perform some private administrative task.
 *
 * @param {string} user_id
 *   The user id that is performing some administrative task
 * @param {string} room_id
 *   The room id for which this administrative task is performed
 * @param {string} body
 *   The body of the message wishing to be sent.
 * @param {string} body_text
 *   The body of the message wishing to be sent in plain text.
 */

async function getDirectRoom(user_id, command) {
  const invited_dm_rooms = Object.keys(client.store.rooms).filter((room) => {
    const loaded_room = client.getRoom(room)
    if (!loaded_room) return false
    // We are strict here, if the room is not with just two users it won't be accepted as a safe
    // way to send tokens for commands set.
    return loaded_room.getMyMembership() === 'invite' || loaded_room.getMyMembership() === 'join'
      && loaded_room.getMember(user_id) && loaded_room.getMember(client.getUserId())
      && loaded_room.getInvitedAndJoinedMemberCount() == 2
  })
  if (invited_dm_rooms[0]) {
    command(invited_dm_rooms[0])
  } else {
    console.log('Creating direct room')
    client.createRoom({
      preset: 'trusted_private_chat',
      invite: [user_id],
      is_direct: true
    }).then((created_room_id) => {
      command(created_room_id)
    })
  }
}

async function sendDirectNotice(user_id, body, body_text) {
  const invited_dm_rooms = Object.keys(client.store.rooms).filter((room) => {
    const loaded_room = client.getRoom(room)
    if (!loaded_room) return false
    // We are strict here, if the room is not with just two users it won't be accepted as a safe
    // way to send tokens for commands set.
    return loaded_room.getMyMembership() === 'invite' || loaded_room.getMyMembership() === 'join'
      && loaded_room.getMember(user_id) && loaded_room.getMember(client.getUserId())
      && loaded_room.getInvitedAndJoinedMemberCount() == 2
  })
  if (invited_dm_rooms[0]) {
    sendNotice(invited_dm_rooms[0], body, body_text)
  } else {
    console.log('Creating direct room')
    client.createRoom({
      preset: 'trusted_private_chat',
      invite: [user_id],
      is_direct: true
    }).then((created_room_id) => {
      sendNotice(created_room_id, body, body_text)
    })
  }
}

start()
