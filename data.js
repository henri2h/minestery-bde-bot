const fs = require('fs');

const dataFileName = "data/data.json";
const wallFileName = "data/wall.json";

const commandList = "data/commandsList.json";
const garnitureCrepe = 'data/garnitureCrepe.json'

const sqlite3 = require('sqlite3').verbose();

const tableInitialisationSql = `CREATE TABLE IF NOT EXISTS commandes ( id TEXT NOT NULL UNIQUE, sender TEXT NOT NULL, display_name TEXT, time INT, crepe TEXT, quantity INT, order_status TEXT, addresse TEXT, remarque TEXT)`;

const addCommandSql = `INSERT INTO commandes VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
const setCommandStatusSql = `UPDATE commandes SET order_status = ? WHERE id = ?`;
const getCommandSql = `SELECT * FROM commandes WHERE id = ?`;
const getCommandAfterTimeRefSql = `SELECT * FROM commandes WHERE time >= ? ORDER BY time DESC`;
const getCommandNumberAfterTimeRefSql = `SELECT SUM(quantity) FROM commandes WHERE time >= ? ORDER BY time DESC`;
const getCommandWithIdAfterTimeRefSql = `SELECT * FROM commandes WHERE id = ? AND time >= ? ORDER BY time DESC`;
const getCommandNumberWithIdAfterTimeRefSql = `SELECT SUM(quantity) FROM commandes WHERE id = ? AND time >= ? ORDER BY time DESC`;
const getCommandWithIdAndStatusAfterTimeRefSql = `SELECT * FROM commandes WHERE id = ? AND order_status = ? AND time >= ? ORDER BY time DESC`;
const getCommandNumberWithIdAndStatusAfterTimeRefSql = `SELECT SUM(quantity) FROM commandes WHERE id = ? AND order_status = ? AND time >= ? ORDER BY time DESC`;

const debug = true;

module.exports = class Data {

  constructor() {
    this.db = new sqlite3.Database('./data/commandes.db', (err) => {
      if (err) {
        return console.error(err.message);
      } else if (debug) {
        console.log('Connected to SQlite database.');
      }
    });

    // Create table if not exist
    this.db.run(tableInitialisationSql, [], (err) => Data.errorResolution(err, `Table initialised !`));
  }


  /* GENERIC FUNCTIONS */

  saveData(data, filename) {
    fs.writeFile(filename, JSON.stringify(data), (err) => { if (err) console.log(err) })
  }

  getData(filename) {
    if (fs.existsSync(filename)) {
      return JSON.parse(fs.readFileSync(filename))
    }
    return JSON.parse('{}')
  }

  /* NEWS */  
  saveNews(data) {
    this.saveData(data, dataFileName)
  }

  getNews() {
    return this.getData(dataFileName)
  }


  /* Save the wall data */
  saveWall(data) {
    this.saveData(data, wallFileName)
  }

  getWall() {
    return this.getData(wallFileName)
  }

  /* OLD CREPE SAVE */

  writeCrepeCommand(data) {
    this.saveData(data, commandList)
  }

  /* GARNITURE CREPE */

  saveGarnitureCrepe(data) {
    this.saveData(data, garnitureCrepe)
  }

  getGarnitureCrepe() {
    return this.getData(garnitureCrepe)
  }

  /* NEW CREPE */

  static errorResolution(err, noErrorDebug) {
    if (err) {
      console.error(err.message)
      return true
    } else if (debug) {
      console.log(noErrorDebug)
    }
    return false
  }

  // callback(err, rows)
  getCommandAfterTimeRef(timestamp, callback) {
    this.db.all(getCommandAfterTimeRefSql, [timestamp], (err, results) => callback(err, results))
  }

  // callback(err, rows)
  getCommandWithIdAfterTimeRef(id, timestamp, callback) {
    this.db.all(getCommandWithIdAfterTimeRefSql, [id, timestamp], (err, results) => callback(err, results));
  }

  // callback(err, rows)
  getCommandWithIdAndStatusAfterTimeRef(id, status, timestamp, callback) {
    this.db.all(getCommandWithIdAndStatusAfterTimeRefSql, [id, status, timestamp], (err, results) => callback(err, results));
  }


  // callback(err, rows)
  getCommandNumberAfterTimeRef(timestamp, callback) {
    this.db.all(getCommandNumberAfterTimeRefSql, [timestamp], (err, rows) => callback(err, rows))
  }

  // callback(err, rows)
  getCommandNumberWithIdAfterTimeRef(id, timestamp, callback) {
    this.db.all(getCommandNumberWithIdAfterTimeRefSql, [id, timestamp], (err, rows) => callback(err, rows));
  }

  // callback(err, rows)
  getCommandNumberWithIdAndStatusAfterTimeRef(id, status, timestamp, callback) {
    this.db.all(getCommandNumberWithIdAndStatusAfterTimeRefSql, [id, status, timestamp], (err, rows) => callback(err, rows));
  }



  // callback(err, result)
  getCommand(id, callback) {
    this.db.get(getCommandSql, [id], (err, result) => callback(err, result))
  }


  // callback(err)

  addCommand(data, callback) {
    var counter = 0
    for (const [key, value] of Object.entries(data['crepe'])) {
      counter += value
    }
    this.db.run('INSERT INTO commandes(id, sender, display_name, time, crepe, quantity, order_status, addresse, remarque, contact, delevery_date, livraison, livraison_aix, command_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [data['id'], data['user_id'], data['user'], data['time'], JSON.stringify(data['crepe']), counter, data['order_status'], data["addresse"], data["remarque"], data["contact"], data["delevery_date"], data["livraison"], data["livraison_aix"], data["command_type"]], (err) => callback(err))
  }
  getCommands(callback) {
    this.db.all("SELECT * FROM commandes;", [], (err, result) => callback(err, result))
  }
  setCommandStatus(id, status, callback) {
    this.db.run(setCommandStatusSql, [status, id], (err) => callback(err))
  }

  addRemarque(data, callback) {
    this.db.run('INSERT INTO remarques (id, sender, title, body, type, time) VALUES (?, ?, ?, ?, ?, ?)', [data['id'], data['user'], data['title'], data['body'], data["type"], data["time"]], (err) => callback(err))

  }

}
