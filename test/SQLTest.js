const Data = require('../data.js')
const data = new Data();


const obj1 = {
    "id" : "test",
    "sender" : "xxxx",
    "display_name" : "le méchant",
    "time" : 1500,
    "crepe" : ["nutella", "sucre"],
    "order_status" : "processing"
};

const obj2 = {
    "id" : "afbieappg",
    "sender" : "yyyy",
    "display_name" : "le gentil",
    "time" : 1700,
    "crepe" : ["fraises", "banane", "gnome"],
    "order_status" : "sent"
}


main = async function() {


		
	abcde = function(err, results) {
		if (err)
			console.error(err.message);
		console.log(results);
	}

	data.addCommand(obj1, abcde);
	data.addCommand(obj2, abcde);

	data.getCommand("test", abcde);
	
	data.setCommandStatus("afbieappg", "artichaud", abcde);

	data.getCommand("afbieappg", abcde);

	data.getCommandAfterTimeRef(1600, abcde);
	
	await data.getCommandNumberAfterTimeRef(1200, abcde);

	data.getCommandAfterTimeRef(0, abcde);
}

main();
