
// logger
const winston = require('winston');
const createLogger = winston.createLogger;

const format = winston.format;
const transports = winston.transports;

const colorize = require('json-colorizer')

const myFormat = format.printf(info => {
  const { timestamp: tmsmp, level, message, stack, ...rest } = info

  // format message
  var messageIn = message
  if (typeof message === 'object') {
    messageIn = JSON.stringify(message)
  }
  let log = `${tmsmp} - ${level}:\t${messageIn}`

  // Only if there is an error
  if (stack !== undefined) log = `${log}\n ${stack}`
  // Check if rest is object
  if (!(Object.keys(rest).length === 0 && rest.constructor === Object)) {
    log = `${log}\n${colorize(JSON.stringify(rest, null, 2))}`
  }
  return log
})

Date.prototype.addHours = function (h) {
  this.setHours(this.getHours() + h);
  return this;
}

var previousCommand = {
  //  lasagne: 83,
  //  lasagne_vege: 13,
  kebab: 142
}

const logger = createLogger({
  level: 'info',
  format: format.combine(
    format.colorize(),
    format.timestamp(),
    myFormat
    // format.printf(inlineFormat)
  ),
  transports: [
    //
    // - Write all logs with level `error` and below to `error.log`
    // - Write all logs with level `info` and below to `combined.log`
    //
    new transports.File({
      filename: 'log/error.log',
      level: 'error',
      handleExceptions: true,
      format: winston.format.simple()
    }),
    new transports.File({
      filename: 'log/combined.log',
      handleExceptions: true,
      format: winston.format.simple()
    })
  ]
})

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
  logger.add(
    new transports.Console({
      timestamp: true,
      handleExceptions: true
    })
  )
}

const { spawn } = require('child_process');

var exec = require('child_process').exec;

const express = require('express')
const app = express()
const port = 3001

const request = require('request');

const Data = require('./data.js')
const data = new Data();

const { LocalStorage } = require('node-localstorage');
const localStorage = new LocalStorage('./local_storage/');

const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));

var call = 0

function commande_bash(text, liste) {
  const fs = require('fs');

  async function sendMatrix() {
    try {

      var room = "!XKukcKItjDudPGrzhu%3Acarnot.cc"

      if (liste == "astronomines") {
        room = "!TFEyyhNRzpOzmyzFZB:carnot.cc"
      }
      else if (liste == "minesverick") {
        room = "!KThPZxrwCSynlHNbGK:carnot.cc"
      }
      else {
        text = "Debug : " + liste + "\n" + text
      }

      var bearerJson = JSON.parse(fs.readFileSync("credentials.json"))

      var req = await fetch("https://matrix.carnot.cc/_matrix/client/r0/rooms/" + room + "/send/m.room.message/m" + Date.now(), {
        "credentials": "include",
        "headers": {
          "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:97.0) Gecko/20100101 Firefox/97.0",
          "Accept": "application/json",
          "Accept-Language": "en-US,en;q=0.5",
          "Content-Type": "application/json",
          "Sec-Fetch-Dest": "empty",
          "Sec-Fetch-Mode": "cors",
          "Sec-Fetch-Site": "cross-site",
          "Authorization": "Bearer " + bearerJson["access_token"]
        },
        "body": JSON.stringify({
          "body": text,
          "msgtype": "m.text"
        }),
        "method": "PUT",
        "mode": "cors"
      });
      let reqText = await req.text();
      console.log(reqText);

    } catch (err) {
      console.error(err);
    };
  };
  call = sendMatrix();
}


function roundDate(date) {
  var d = new Date(date)
  var trunc = d.getMinutes()
  d.setMinutes(trunc - trunc % 5)
  d.setSeconds(0);
  d.setMilliseconds(0);
  return d.getTime()
}

const config = require('./config.js');
app.use(express.json());

app.get('/bot', (req, res) => {
  var dt = data.getNews();
  res.json(dt);
})

app.get('/wall', (req, res) => {
  res.json({ "items": getWall().items });
})


// Config
app.get('/config', (req, res) => {
  var dt = config.getConfig();
  dt.listes = JSON.parse(localStorage.getItem("commandesListe"))
  res.json({ "success": true, "result": dt });
})

app.post('/config', (req, res) => {

  const fs = require('fs');

  const secretKey = "6LcjU3IeAAAAAGgYoqIOFOQaSxUSt_nu2ISS3PD3"
  const verificationURL = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body.token + "&remoteip=" + req.connection.remoteAddress;

  request(verificationURL, function (error, response, body) {
    body = JSON.parse(body);

    if (body.success !== undefined && !body.success) {
      return res.json({ "success": false, "err": "Failed captcha verification" });
    }

    logger.info('config')
    logger.info(req.body.config)

    var config_token = JSON.parse(fs.readFileSync("credentials.json")).config_token;

    // check token
    if (req.body.config_token === config_token) {
      config.saveConfig(req.body.config, (err) => {
        if (err) return res.json({ "success": false, "err": err });
        return res.json({ "success": true });
      })
    } else {
      logger.info("Wrong token")
      logger.info(req.body)
      return res.json({ "success": false, "err": "Wrong token" });
    }
  });


})


// get affluence
app.get('/aff', (req, res) => {
  var count = JSON.parse(localStorage.getItem("commandesCount"))
  var liste = JSON.parse(localStorage.getItem("commandesListe"))
  res.json({ "success": true, "count": count.time_type, "total": count.total, "listes": liste });
})


app.post('/orders', (req, res) => {
  if (req.body["token"] == "secret") {
    data.getCommands((err, result) => {
      var data = []
      var date = new Date().getTime() - 1000 * 60 * 5

      var count = {
        total: {
          type: {},
          "commandItemTotal": 0,
          "commandTotal": 0
        },
        time: {}
      }

      for (const index in result) {
        var timestamp = roundDate(result[index].delevery_date)
        var elem = result[index]
        var type = elem.command_type

        // compte le nombre de commandes
        count.total.commandTotal += 1


        // add time count
        if (timestamp > 1615717827000) {
          if (count.time_type === undefined) count.time_type = {}
          if (!(type in count.time_type)) count.time_type[type] = {}
          if (!(timestamp in count.time_type[type])) count.time_type[type][timestamp] = 0
          count.time_type[type][timestamp] += 1
        }
        // compte le nombre de commandes par type
        if (result[index].command_type in count.total.type)
          count.total.type[result[index].command_type] += 1
        else count.total.type[result[index].command_type] = 1

        // catégorise les commandes de crêpe
        if (result[index].delevery_date == "now") result[index].delevery_date = result[index].time

        const crepes = JSON.parse(result[index].crepe)

        for (const crepePos in crepes) {
          if (crepes[crepePos] == true) crepes[crepePos] = 1
          const crepeCount = parseInt(crepes[crepePos])

          // if is valid count
          if (!isNaN(crepeCount)) {
            count.total.commandItemTotal += crepeCount

            if (!(crepePos in count.total)) count.total[crepePos] = 0
            count.total[crepePos] += crepeCount


            // add time count
            if (!(crepePos in count.time)) count.time[crepePos] = {}
            if (!(timestamp in count.time[crepePos])) count.time[crepePos][timestamp] = 0
            count.time[crepePos][timestamp] += crepeCount

          }
        }
        if (result[index].order_status !== 'flight' || result[index].delevery_date > date || result[index].time > date || req.body.all) data.push(result[index])
      }

      count.total.type.kebab -= previousCommand.kebab
      //count.total.lasagne -= previousCommand.lasagne
      //count.total.lasagne_vege -= previousCommand.lasagne_vege

      localStorage.setItem("commandesCount", JSON.stringify(count))


      return res.json({
        "success": true, "err": err, "result": data, "count": count.total
      })
    })
  }
  else {
    logger.info("Wrong token")
    logger.info(req.body)
    return res.json({ "success": false, "err": "Wrong token" });
  }
})



app.put('/orders', (req, res) => {
  if (req.body["token"] == "secret") {
    logger.info('update')
    logger.info(req.body)
    data.setCommandStatus(req.body.id, req.body.order_status, (err) => {
      if (err) return res.json({ "success": false, "err": err });
      return res.json({ "success": true });
    })
  }
  else {
    logger.info("Wrong token")
    logger.info(req.body)
    return res.json({ "success": false, "err": "Wrong token" });
  }
})
app.post('/order', (req, res) => {
  var liste = req.body["liste"];

  var commandesCount = JSON.parse(localStorage.getItem("commandesCount"))
  if (commandesCount == undefined) { commandesCount["total"] = {}; }

  var commandesListe = undefined;
  try {
    commandesListe = JSON.parse(localStorage.getItem("commandesListe"))
  } catch (error) {
    console.error(error);
  }
  // create the liste data
  if (commandesListe == undefined) { commandesListe = {}; }
  if (commandesListe[liste] == undefined) { commandesListe[liste] = 0; }

  let date = new Date().getTime();
  let configIn = config.getConfig()

  req.body["id"] = "site_" + date;
  req.body["user_id"] = "website";
  req.body["time"] = date;
  req.body["order_status"] = "sent";
  logger.info(req.body)

  // Get commandes détaille
  var commande_text = "Commande de " + req.body["user"] + "\n";
  if (req.body.livraison) {
    commande_text += "Addresse : " + req.body["addresse"] + "\n"
  }
  else {
    commande_text += "Livraison : Sur place\n";
  }

  if (req.body["delevery_date"] < date) req.body["delevery_date"] = date
  if (req.body.delevery_date < configIn[req.body.command_type].minDate || req.body.delevery_date > configIn[req.body.command_type].maxDate) return res.json({ "success": false, "err": "La date de livraison est en dehors des dates de l'operation " + req.body.command_type });


  var timestamp = roundDate(req.body.delevery_date)
  var commandes = 0

  var crepe = req.body["crepe"]
  for (const item in crepe) {

    if (isNaN(crepe[item]) || crepe[item] < 0) {
      return res.json({ "success": false, "err": "Bien tenté, mais l'entrée " + item + " n'est pas un nombre. Au passage, un nomber de commande négatif... c'est pas ouf." })
    }
    // compteur total
    if (!(item in commandesCount.total)) commandesCount.total[item] = 0



    commandesCount.total[item] += crepe[item]

    var name = item
    if (configIn["crepe"]["choices"][item] !== undefined) {
      name = configIn["crepe"]["choices"][item].name;
    }

    commande_text += name + " : " + String(crepe[item]) + "\n"

    // compteur pour la commande
    commandes += crepe[item]

    if (item in configIn.maxCount) {
      if (commandesCount.total[item] > configIn.maxCount[item]) return res.json({ "success": false, "err": "Le nombre maximum de " + name + " à été atteint" })
    }

    // test maxItem
    var maxItem = configIn["crepe"].choices[item].max;
    if (maxItem) {
      if (crepe[item] > maxItem) return res.json({ "success": false, "err": "Le nombre maximum de " + name + " par commande est de " + maxItem })
    }
  }

  // check affluence
  // compteur total rate limit par tranche
  var time_type = commandesCount.time_type
  if (time_type == undefined) { time_type = {}; }
  var type = req.body.command_type
  if (!configIn[type].enabled)
    return res.json({ "success": false, "err": "Cette commande n'est pas actuellement disponible ;)" });


  // create counter by type
  if (!(type in commandesCount.total.type)) commandesCount.total.type[type] = 0
  commandesCount.total.type[type] += 1

  // compteur par type
  if (type in configIn.maxType) {
    if (commandesCount.total.type[type] > configIn.maxType[type]) return res.json({ "success": false, "err": "On n'a plus de " + type })
  }


  if (!(type in time_type)) time_type[type] = {}
  if (!(timestamp in time_type[type])) time_type[type][timestamp] = 0
  time_type[type][timestamp] += 1 // increment count

  // compteur par timestamp
  if (type in configIn.maxAff && timestamp in time_type[type]) {
    if (time_type[type][timestamp] > configIn.maxAff[type]) {
      return res.json({ "success": false, "err": "Il n'y a plus de places disponible dans ce creneau. Réessayez au crénau suivant (5min plus tard)" })
    }
  }

  if (commandes > 15) {
    return res.json({ "success": false, "err": "Une commande de 15 crèpes maximum est authorisée" });
  }
  else if (commandes == 0) {
    return res.json({ "success": false, "err": "Vous ne voulez pas commander au moins une crêpe ?" });
  }

  // add counter to liste
  commandesListe[liste] = commandesListe[liste] + commandes;


  if (req.body.livraison && req.body["addresse"] == "")
    return res.json({ "success": false, "err": "L'adresse est vide" });
  if (req.body["user"] == "")
    return res.json({ "success": false, "err": "Le nom est vide" });

  if (req.body["remarque"] != "") {
    commande_text += "\nRemarque : " + req.body["remarque"] + "\n"
  }

  // command date time. We add one hour to fix for the UTC time. Dirty fix
  var formattedTime = new Date(req.body.delevery_date).addHours(1).toTimeString().replace(/.*(\d{2}:\d{2}):.*/, "$1").replace(":", "h");

  commande_text += "Pour " + formattedTime + "\n"

  logger.info(commande_text)

  const secretKey = "6LcjU3IeAAAAAGgYoqIOFOQaSxUSt_nu2ISS3PD3"
  const verificationURL = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body.token + "&remoteip=" + req.connection.remoteAddress;

  request(verificationURL, function (error, response, body) {
    body = JSON.parse(body);

    if (body.success !== undefined && !body.success) {
      return res.json({ "success": false, "err": "Failed captcha verification" });
    }

    data.addCommand(req.body, (err) => {
      if (err) {
        logger.info(err);
        return res.json({ "success": false, "err": "Une erreur est survenue, veuillez réessayer plus tard.", "date": date })
      }

      commande_bash(commande_text, liste)
      localStorage.setItem("commandesCount", JSON.stringify(commandesCount))
      localStorage.setItem("commandesListe", JSON.stringify(commandesListe)) // compteur des commandes de chaque liste
      return res.json({ "success": true, "date": date })


      // save data
    });
  });


})


app.post('/remarques', (req, res) => {
  let date = new Date().getTime();
  req.body["time"] = date

  const secretKey = "6LcjU3IeAAAAAGgYoqIOFOQaSxUSt_nu2ISS3PD3"

  const verificationURL = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body.token + "&remoteip=" + req.connection.remoteAddress;

  request(verificationURL, function (error, response, body) {
    body = JSON.parse(body);


    if (body.success !== undefined && !body.success) {
      return res.json({ "success": false, "err": "Failed captcha verification" });
    }

    data.addRemarque(req.body, (err) => {
      if (err) {
        logger.info(err);
        return res.json({ "success": false, "err": "Une erreur est survenue, veuillez réessayer plus tard.", "date": date })
      }
      res.json({ "success": true, "date": date })
    });
  });
})


var wallData = null;
function getWall() {

  if (wallData == null) {
    wallData = data.getWall();
  }

  if (wallData.items === undefined) { wallData.items = []; }

  return wallData;
}

var timeoutSaveWall = null;

function saveWall(dataIn) {
  // sort data
  dataIn.items.sort(function (a, b) {
    return b.count - a.count;
  });

  wallData = dataIn;

  if (timeoutSaveWall != null) {
    clearTimeout(timeoutSaveWall);
  }

  timeoutSaveWall = setTimeout(() => {
    console.log("Save wall");
    data.saveWall(wallData);
  }, 1500);
}


app.post('/wall', (req, res) => {
  var wData = getWall();

  if (!req.body.title || !req.body.text || !req.body.username) {
    return res.json({ "success": false, "err": "Tout les champs doivent être remplis." });
  }

  wData.items.push({ // prevent the user to store arbitrary data
    title: req.body.title,
    text: req.body.text,
    username: req.body.username,
    count: 1,
    id: new Date().getTime()
  });



  saveWall(wData);
  return res.json({ "success": true });
})

app.put('/wall/:id', (req, res) => {
  var wData = getWall();
  var id = req.params.id;

  for (let index = 0; index < wData.items.length; index++) {
    const element = wData.items[index];

    if (element.id == id) {
      element.count += 1;
      wData.items[index] = element;

      saveWall(wData);
      return res.json({ "success": true });
    }

  }


  return res.json({ "success": false });
})

app.on('error', (appErr, appCtx) => {
  logger.error('app error', appErr.stack);
  logger.error('on url', appCtx.req.url);
  logger.error('with headers', appCtx.req.headers);
});

app.listen(port, () => {
  logger.info(`Minestrix http server listening at http://localhost:${port}`)
})

