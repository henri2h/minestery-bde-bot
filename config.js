const fs = require('fs');
var filename = "data/config.json"

var config = null
const getConfig = () => {
  if (config == null) {
    if (fs.existsSync(filename)) {
      return JSON.parse(fs.readFileSync(filename))
    }
    return JSON.parse('{}')
  }
  return config
}

const saveConfig = (data, callback) => {
  fs.writeFile(filename, JSON.stringify(data), (err) => callback(err));
}
exports.getConfig = getConfig;
exports.saveConfig = saveConfig;